package com.baidu.ueditor.web;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.ueditor.ActionEnter;

@Controller
public class UEditorController {
	@RequestMapping("/config")
	public @ResponseBody void getConfigInfo(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.setContentType("application/json");
			String exec = new ActionEnter(request).exec();
			PrintWriter writer = response.getWriter();
			writer.write(exec);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
